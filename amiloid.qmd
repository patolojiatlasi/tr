# Amiloidoz (Amiloid Birikimi)

## Kristal Viyole

Damar duvarlarında amiloid birikimi

<https://pathologyatlas.github.io/amyloid/crystalviolet.html>

Mikroskopik görüntüleri inceleyin:

<iframe src="https://pathologyatlas.github.io/amyloid/crystalviolet.html" style="height:400px;width:100%;" data-external="1">

</iframe>

## Congo Red

Congo Red stain for amyloidosis

<https://pathologyatlas.github.io/congored/congored.html>

See Microscopy with viewer:

<iframe src="https://pathologyatlas.github.io/congored/congored.html" style="height:400px;width:100%;" data-external="1">

</iframe>

## Congo Red Birefringence

<iframe width="560" height="315" src="https://www.youtube.com/embed/U9glkfQLTm4" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>

</iframe>
