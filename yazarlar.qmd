# Yazarlar ve Katkıda Bulunanlar {.unnumbered}

## Derleyen:

-   [Serdar Balcı](https://www.serdarbalci.com)

## Katkıda Bulunanlar:

[**Memorial Patoloji**](https://patoloji.memorial.com.tr/) **Hekimleri**

-   [Ilknur Turkmen](https://www.memorial.com.tr/en/doctors/ilknur-turkmen-1975)

-   [Gülen Bülbül Doğusoy](https://www.memorial.com.tr/doktorlar/gulen-bulbul-dogusoy)

-   [Fatma Aktepe](https://www.memorial.com.tr/doktorlar/fatma-aktepe)

-   [Türkan Atasever Rezanko](https://www.memorial.com.tr/doktorlar/turkan-atasever-rezanko)

-   [Pembe Gül Güneş](https://www.memorial.com.tr/doktorlar/pembe-gul-gunes)

-   [Şemsi Yıldız](https://www.memorial.com.tr/doktorlar/semsi-yildiz)

-   [Serdar Balcı](https://www.memorial.com.tr/doktorlar/serdar-balci)

-   [Sezen Koçarslan](https://www.memorial.com.tr/doktorlar/sezen-kocarslan)

-   [Yıldırım Karslıoğlu](https://www.memorial.com.tr/doktorlar/yildirim-karslioglu)

-   [Mehtat Uz Ünlü](https://www.memorial.com.tr/doktorlar/mehtat-uz-unlu)

-   [Murat Oktay](https://www.memorial.com.tr/doktorlar/murat-oktay)

-   [Deniz Bayçelebi](https://www.memorial.com.tr/doktorlar/deniz-baycelebi)

-   [Emre Karakök](https://www.memorial.com.tr/doktorlar/emre-karakok)

-   [Zuhal Kuş Sılav](https://www.memorial.com.tr/doktorlar/zuhal-kus-silav)

-   [Fatma Gülgün Sade Koçak](https://www.memorial.com.tr/doktorlar/fatma-gulgun-sade-kocak)

-   [Zeynep Pehlivanoğlu](https://www.memorial.com.tr/doktorlar/zeynep-pehlivanoglu)

[**Memorial Patoloji**](https://patoloji.memorial.com.tr/) **Teknikerleri**

-   [Emrah Uça]

-   [Şevin Elif Şanioğlu](https://www.linkedin.com/in/%C5%9Fevin-elif-%C5%9Fanio%C4%9Flu-99449a1b0/)

-   [Baset Sığırcı](https://www.linkedin.com/in/baset-s%C4%B1%C4%9F%C4%B1rc%C4%B1-aa2406141/)

-   [Rabia Öztürk](https://www.linkedin.com/in/rabia-%C3%B6zt%C3%BCrk-4989b3151/)
